﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Data.Entities
{
    public class UserDetailsEntity
    {
        [Key]
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string SubDomain { get; set; }

        public string TimeZone { get; set; }

        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }

        public int EmployeeNumber { get; set; }

        public string Department { get; set; }

        public string Role { get; set; }

        public string Country { get; set; }
    }
}
