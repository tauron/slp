﻿using SLPortal.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Data.Entities
{
    public class PrelaunchEntity
    {
        public string Id { get; set; }
        public string SubDomain { get; set; }
        public string Email { get; set; }
        public Prelaunch ToDomain()
        {
            return new Prelaunch()
            {
                Id = this.Id,
                SubDomain = this.SubDomain,
                Email = this.Email
            };
        }

    }
}
