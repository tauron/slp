﻿using SLPortal.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Data
{
    public partial class RepositoryContext : DbContext
    {
        public RepositoryContext()
            : base("name=SlpConnection")
        {
        }

        public virtual DbSet<UserDetailsEntity> UserDetails { get; set; }
        public virtual DbSet<PrelaunchEntity> Prelaunchs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDetailsEntity>().ToTable("UserDetails");
            modelBuilder.Entity<PrelaunchEntity>().ToTable("Prelaunch");

        }


    }
}
