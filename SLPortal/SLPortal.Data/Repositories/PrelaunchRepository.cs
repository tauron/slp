﻿using SLPortal.Data.Entities;
using SLPortal.Domain;
using SLPortal.Domain.Repositories;
using System;
using System.Linq;

namespace SLPortal.Data.Repositories
{
    public class PrelaunchRepository : IPrelaunchRepository
    {
        private RepositoryContext context;
        public PrelaunchRepository()
        {
            context = new RepositoryContext();
        }

        public void Add(Prelaunch prelaunch)
        {
            if (prelaunch != null)
            {
                context.Prelaunchs.Add(new PrelaunchEntity()
                {
                    Id = Guid.NewGuid().ToString(),
                    SubDomain = prelaunch.SubDomain,
                    Email = prelaunch.Email
                }
                );
                context.SaveChanges();
            }
        }

        public bool verifExistEmail(string email)
        {
            var verif = false;
            if (!String.IsNullOrEmpty(email))
            {
                var prelauchs = context.Prelaunchs.Where(p => p.Email == email).ToList();
                if (prelauchs.Count > 0)
                {
                    verif = true;
                }
            }
            return verif;
        }

        public bool verifExistSubDomain(string subDomain)
        {
            var verif = false;
            if (!String.IsNullOrEmpty(subDomain))
            {
                var prelauchs = context.Prelaunchs.Where(p => p.SubDomain == subDomain).ToList();
                if (prelauchs.Count > 0)
                {
                    verif = true;
                }
            }
            return verif;
        }
    }
}
