﻿using SLPortal.Data.Entities;
using SLPortal.Domain;
using SLPortal.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Data.Repositories
{
    public class UserDetailRepository : IUserDetailsRepository
    {
        private RepositoryContext context;
        public UserDetailRepository()
        {
            context = new RepositoryContext();
        }

        public void Add(UserDetails user)
        {
            if (user != null)
            {
                context.UserDetails.Add(new UserDetailsEntity()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    CompanyName = user.CompanyName,
                    Country = user.Country,
                    Department = user.Department,
                    PhoneNumber = user.PhoneNumber,
                    EmployeeNumber = user.EmployeeNumber,
                    Role = user.Role,
                    SubDomain = user.SubDomain,
                }
                );
                context.SaveChanges();
            }
        }
    }
}
