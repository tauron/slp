﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Domain
{
    public class Prelaunch
    {
        public string Id { get; set; }
        public string SubDomain { get; set; }
        public string Email { get; set; }
    }
}
