﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Domain.Repositories
{
    public interface IUserDetailsRepository
    {
        void Add(UserDetails user);
    }
}
