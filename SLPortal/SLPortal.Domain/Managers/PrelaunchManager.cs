﻿using SLPortal.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Domain.Managers
{
    public class PrelaunchManager : IPrelaunchManager
    {
        private readonly IPrelaunchRepository _repository;

        public PrelaunchManager(IPrelaunchRepository repository)
        {
            _repository = repository;
        }
        public void Add(Prelaunch prelaunch)
        {
            _repository.Add(prelaunch);
        }

        public bool verifExistEmail(string email)
        {
            return _repository.verifExistEmail(email);
        }

        public bool verifExistSubDomain(string subDomain)
        {
            return _repository.verifExistSubDomain(subDomain);
        }
    }
}
