﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Domain.Managers
{
    public interface IUserDetailsManager
    {
        void Add(UserDetails user);
    }
}
