﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Domain.Managers
{
   public  interface IPrelaunchManager
    {
        void Add(Prelaunch prelaunch);
        bool verifExistSubDomain(string subDomain);
        bool verifExistEmail(string email);

    }
}
