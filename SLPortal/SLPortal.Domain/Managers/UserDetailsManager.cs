﻿using SLPortal.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLPortal.Domain.Managers
{
    public class UserDetailsManager : IUserDetailsManager
    {
        private readonly IUserDetailsRepository _repository;

        public UserDetailsManager(IUserDetailsRepository repository)
        {
            _repository = repository;
        }
        public void Add(UserDetails user)
        {
            _repository.Add(user);
        }
    }
}
