﻿using SLPortal.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLPortal.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public void AddPrelaunch(Prelaunch prelaunch)
        {
            if (prelaunch != null)
            {
                PrelaunchManager.Add(prelaunch);
            }
        }

        //verif exist subDomain for add
        [AllowAnonymous]
        public JsonResult IsSubDomainAvailable(string subDomain)
        {
            bool status = !PrelaunchManager.verifExistSubDomain(subDomain);
            return Json(new { result = status }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult IsEmailAvailable(string email)
        {
            bool status = !PrelaunchManager.verifExistEmail(email);
            return Json(new { result = status }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}