﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SLPortal.Web.Ioc;
using SLPortal.Domain.Managers;
using Microsoft.Practices.Unity;

namespace SLPortal.Web.Controllers
{
    public class BaseController : Controller
    {
        //private IPrelaunchManager _prelaunchManager;
        //public IPrelaunchManager PrelaunchManager
        //{
        //    get
        //    {
        //            return _prelaunchManager ?? HttpContext.GetOwinContext().Get<IPrelaunchManager>();
        //    }
        //    set
        //    {
        //        _prelaunchManager = value;
        //    }
        //}
        private IPrelaunchManager _prelaunchManager;
        public IPrelaunchManager PrelaunchManager
        {
            get
            {
                if (_prelaunchManager == null)
                {
                    _prelaunchManager = MvcUnityContainer.Container.Resolve<IPrelaunchManager>();
                }
                return _prelaunchManager;
            }
            set
            {
                _prelaunchManager = value;
            }
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
    }
}