﻿using SLPortal.Web.App_Start;
using SLPortal.Web.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SLPortal.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }


        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            //var logService = MvcUnityContainer.Container.Resolve<ILogService>();
            //logService.Log(exception);

            //var showDefaultErrorPage = ConfigurationManager.AppSettings["ShowDefaultErrorPage"].ToString();
            //if (showDefaultErrorPage == "true")
            //{
            //    Response.Clear();
            //    Server.ClearError();

            //    var routeData = new RouteData();
            //    routeData.Values["controller"] = "Home";
            //    routeData.Values["action"] = "Error";
            //    routeData.Values["exception"] = exception;

            //    IController errorsController = new HomeController();
            //    var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
            //    errorsController.Execute(rc);
            //}
        }
    }
}
