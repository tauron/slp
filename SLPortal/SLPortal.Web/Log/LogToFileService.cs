﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace SLPortal.Web.Log
{
    public class LogToFileService : ILogService
    {
        private LogWriter _logWriter = null;
        public void Log(Exception ex)
        {
            Initialise();
            _logWriter.Write(ex);
        }

        private void Initialise()
        {
            // Formatter
            TextFormatter briefFormatter = new TextFormatter();

            // Trace Listener
            var flatFileTraceListener = new FlatFileTraceListener(ConfigurationManager.AppSettings["LogFilePath"],
                                                                    "----------------------------------------",
                                                                    "----------------------------------------",
                                                                     briefFormatter);

            // Build Configuration
            var config = new LoggingConfiguration();

            config.AddLogSource("DiskFiles", SourceLevels.All, true)
             .AddTraceListener(flatFileTraceListener);

            _logWriter = new LogWriter(config);
        }
    }
}