﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLPortal.Web.Log
{
    public interface ILogService
    {
        void Log(Exception exception);
    }
}