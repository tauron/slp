﻿using System.Web;
using System.Web.Optimization;

namespace SLPortal.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/ui.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/ui.css",
                      "~/Content/animate.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/angularMaterialCss").Include(
                     "~/Content/angular-material/angular-material.css"));

            bundles.Add(new ScriptBundle("~/bundles/angularJS").Include(
                    "~/Scripts/angular.js"));
            bundles.Add(new ScriptBundle("~/bundles/angularMaterialJS").Include(
                 "~/Scripts/angular-animate.js",
                 "~/Scripts/angular-route.js",
                 "~/Scripts/angular-aria.js",
                 "~/Scripts/angular-cookies.js",
                 "~/Scripts/angular-messages.js",
                 "~/Scripts/angular-resource.js",
                 "~/Scripts/angular-sanitize.js",
                 "~/Scripts/angular-touch.js",
                 "~/Scripts/svg-assets-cache.js",
                "~/Scripts/angular-material.js"
               ));
        }
    }
}
