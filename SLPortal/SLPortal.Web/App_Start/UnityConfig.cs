﻿using Microsoft.Practices.Unity;
using SLPortal.Web.Controllers;
using SLPortal.Web.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Unity.Mvc5;
using SLPortal.Web.Log;
using SLPortal.Domain.Managers;
using SLPortal.Domain.Repositories;
using SLPortal.Data.Repositories;

namespace SLPortal.Web.App_Start
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());

            container.RegisterType<IPrelaunchManager, PrelaunchManager>();
            container.RegisterType<IPrelaunchRepository, PrelaunchRepository>();

            container.RegisterType<IUserDetailsManager, UserDetailsManager>();
            container.RegisterType<IUserDetailsRepository, UserDetailRepository>();

            //Log Service
            //container.RegisterType<ILogService, LogToFileService>();

            //DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            MvcUnityContainer.Container = container;
        }
    }
}