﻿prelaunchapplication.service("prelaunAJService", function ($http) {

    this.AddPrelaunch = function (prelaunch) {
        var response = $http({
            method: "post",
            url: window.location.origin + '/Home/AddPrelaunch',
            data: JSON.stringify(prelaunch),
            dataType: "json"
        });
        return response;
    }
});