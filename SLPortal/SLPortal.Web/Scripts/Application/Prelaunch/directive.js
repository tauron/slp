﻿prelaunchapplication.directive('ngUnique', ['$http', function (async) {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            elem.on('blur', function (evt) {
                scope.$apply(function () {
                    var val = elem.val();
                    var req = { "subDomain": val }
                    var ajaxConfiguration = {
                        method: 'POST',
                        url: window.location.origin + '/Home/IsSubDomainAvailable',
                        data: req
                    };
                    async(ajaxConfiguration)
                        .success(function (data, status, headers, config) {
                            ctrl.$setValidity('unique', data.result);
                        });
                });
            });
        }
    }
}]);

prelaunchapplication.directive('ngUniqueEmail', ['$http', function (async) {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            elem.on('blur', function (evt) {
                scope.$apply(function () {
                    var val = elem.val();
                    var req = { "email": val }
                    var ajaxConfiguration = {
                        method: 'POST',
                        url: window.location.origin + '/Home/IsEmailAvailable',
                        data: req
                    };
                    async(ajaxConfiguration)
                        .success(function (data, status, headers, config) {
                            ctrl.$setValidity('uniqueEmail', data.result);
                        });
                });
            });
        }
    }
}]);
