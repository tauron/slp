﻿prelaunchapplication.controller("prelaunCtrl", function ($scope, toaster, prelaunAJService) {


    //Toaster notifications
    $scope.toasterSuccessAdd = {
        type: 'success',
        title: 'Success!',
        text: 'Thanks! Your demo request has been accepted, we will get back to you soon!'
    };
    $scope.toasterErrorAdd = {
        type: 'error',
        title: 'error!',
        text: ' Your subscription has failed'
    };

        function ClearFields() {
        $scope.subDomain = "";
        $scope.email = "";
    }

    $scope.addPrelaunch = function () {

        var Prelaunch = {
            SubDomain: $scope.subDomain,
            Email: $scope.email
        };
        var getPrelaunchData = prelaunAJService.AddPrelaunch(Prelaunch);
        getPrelaunchData.then(function () {
            ClearFields();
            toaster.pop($scope.toasterSuccessAdd.type, $scope.toasterSuccessAdd.title, $scope.toasterSuccessAdd.text);
        }, function () {
            toaster.pop($scope.toasterErrorAdd.type, $scope.toasterErrorAdd.title, $scope.toasterErrorAdd.text);
        });
    }
});