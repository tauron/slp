﻿var time;

function myFunction() {
    time = setTimeout(showPage, 1000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

//changeBG on page load
var images = ['Mock02.jpg', 'device-mockup-7537.jpg', 'bg-triangles-pinkish.png'];
$('#header').css({ 'background-image': 'linear-gradient(to top, rgba(0,0,0,0.0), rgba(0, 0, 0, 0.9)), url(../Content/img/' + images[Math.floor(Math.random() * images.length)] + ')' });