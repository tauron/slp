﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SLPortal.Web.Startup))]
namespace SLPortal.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
