﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLPortal.Web.Ioc
{
    public static class MvcUnityContainer
    {
        public static IUnityContainer Container { get; set; }

        public static void RegisterApplicationUserManager(ApplicationUserManager manager)
        {
            Container.RegisterInstance(typeof(ApplicationUserManager), "ApplicationUserManager", manager, new HierarchicalLifetimeManager());
        }

        public static ApplicationUserManager GetApplicationUserManager()
        {
            return Container.Resolve<ApplicationUserManager>("ApplicationUserManager");
        }
    }
}