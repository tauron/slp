﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLPortal.Web.Models
{
    public static class SystemConstants
    {
        public readonly static string RoleAdmin = "Admin";
        public readonly static string RoleClient = "Client";
    }
}